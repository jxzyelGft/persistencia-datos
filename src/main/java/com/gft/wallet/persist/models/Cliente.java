package com.gft.wallet.persist.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Document(collection = "cliente")
public class Cliente {

	@Id
	private String id;

	private String nombreCompleto;

	private String correo;

	private String clave;

}
