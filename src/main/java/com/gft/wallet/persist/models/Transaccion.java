package com.gft.wallet.persist.models;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "transaccion")
public class Transaccion {

	private String id;

	@DateTimeFormat(iso = ISO.DATE, pattern = "yyyy-MM-dd")
	private LocalDate fecha = LocalDate.now();

	@DateTimeFormat(iso = ISO.TIME, pattern = "HH:mm:ss")
	private LocalTime hora = LocalTime.now();

	private String concepto;

	private BigDecimal importe;

	private String referencia;

	private Producto producto;
}
