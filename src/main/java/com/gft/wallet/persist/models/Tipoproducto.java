package com.gft.wallet.persist.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "tipoproducto")
public class Tipoproducto {

	@Id
	private String id;

	private String tipo;

	private String nombre;

	private Banco banco;

}
