package com.gft.wallet.persist.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.gft.wallet.commons.model.TipoproductoBancoDTO;
import com.gft.wallet.commons.model.TipoproductoDTO;
import com.gft.wallet.persist.models.Banco;
import com.gft.wallet.persist.models.Tipoproducto;

@Mapper(uses = SimpleEntityMapper.class)
public interface TipoproductoMapper {
	TipoproductoMapper INSTANCE = Mappers.getMapper(TipoproductoMapper.class);

	@Mapping(source = "tipo.id", target = "id")
	@Mapping(source = "tipo.nombre", target = "nombre")
	Tipoproducto toTipoproducto(TipoproductoDTO tipo, Banco banco);

	Tipoproducto toTipoproducto(TipoproductoBancoDTO tipo);

	TipoproductoDTO toTipoproductoDTO(Tipoproducto tipo);

	TipoproductoBancoDTO toTipoproductoBancoDTO(Tipoproducto tipo);

}
