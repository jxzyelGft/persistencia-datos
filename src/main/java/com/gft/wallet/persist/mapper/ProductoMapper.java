package com.gft.wallet.persist.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import com.gft.wallet.commons.model.ClienteDTO;
import com.gft.wallet.commons.model.ProductoClienteTipoproductoBancoDTO;
import com.gft.wallet.commons.model.ProductoDTO;
import com.gft.wallet.commons.model.ProductoTipoproductoBancoDTO;
import com.gft.wallet.commons.model.TipoproductoBancoDTO;
import com.gft.wallet.persist.models.Producto;

@Mapper(uses = { TipoproductoMapper.class, SimpleEntityMapper.class })
public interface ProductoMapper {
	ProductoMapper INSTANCE = Mappers.getMapper(ProductoMapper.class);

	@Mapping(source = "tipoproductobanco", target = "tipoproducto")
	Producto toProducto(ProductoDTO prod, TipoproductoBancoDTO tipoproductobanco, ClienteDTO cliente);

	@Mapping(source = "prod.tipoproductobanco", target = "tipoproducto")
	Producto toProducto(ProductoTipoproductoBancoDTO prod, ClienteDTO cliente);

	@Mapping(source = "prod.tipoproductobanco", target = "tipoproducto")
	Producto toProducto(ProductoClienteTipoproductoBancoDTO prod);

	@Mapping(source = "prod.tipoproducto", target = "tipoproductobanco")
	ProductoTipoproductoBancoDTO toProductoTipoproductoBancoDTO(Producto prod);

	@Mapping(source = "prod.tipoproductobanco", target = "tipoproductobanco")
	ProductoTipoproductoBancoDTO toProductoTipoproductoBancoDTO(ProductoClienteTipoproductoBancoDTO prod);

	@Mapping(source = "prod.tipoproducto", target = "tipoproductobanco")
	ProductoClienteTipoproductoBancoDTO toProductoClienteTipoproductoBancoDTO(Producto prod);

	ProductoDTO toProductoDTO(Producto prod);
}
