package com.gft.wallet.persist.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import com.gft.wallet.commons.model.ProductoClienteTipoproductoBancoDTO;
import com.gft.wallet.commons.model.TransaccionDTO;
import com.gft.wallet.commons.model.TransaccionProductoClienteTipoproductoBancoDTO;
import com.gft.wallet.commons.model.TransaccionProductoTipoproductoBancoDTO;
import com.gft.wallet.persist.models.Transaccion;

@Mapper(uses = { TipoproductoMapper.class, SimpleEntityMapper.class, ProductoMapper.class, ProductoMapper.class })
public interface TransaccionMapper {
	TransaccionMapper INSTANCE = Mappers.getMapper(TransaccionMapper.class);

	TransaccionDTO toTransaccionDTO(Transaccion trans);

	Transaccion toTransaccionSimple(TransaccionDTO trans);

	@Mapping(source = "prod", target = "producto")
	Transaccion toTransaccion(TransaccionDTO trans, ProductoClienteTipoproductoBancoDTO prod);

	@Mapping(source = "trans.producto", target = "productotipoproductobanco")
	TransaccionProductoTipoproductoBancoDTO toTransaccionProductoTipoproductoBancoDTO(Transaccion trans);

	@Mapping(source = "trans.producto", target = "productoclientetipoproductobanco")
	TransaccionProductoClienteTipoproductoBancoDTO toTransaccionProductoClienteTipoproductoBancoDTO(Transaccion trans);

}
