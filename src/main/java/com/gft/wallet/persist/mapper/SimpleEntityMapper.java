package com.gft.wallet.persist.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import com.gft.wallet.commons.model.BancoDTO;
import com.gft.wallet.commons.model.ClienteDTO;
import com.gft.wallet.persist.models.Banco;
import com.gft.wallet.persist.models.Cliente;

@Mapper
public interface SimpleEntityMapper {

	SimpleEntityMapper INSTANCE = Mappers.getMapper(SimpleEntityMapper.class);

	ClienteDTO clienteToClienteDTO(Cliente cli);

	Cliente clienteDTOToCliente(ClienteDTO cli);

	BancoDTO bancoToBancoDTO(Banco ban);

	Banco bancoDTOToBanco(BancoDTO ban);

}
