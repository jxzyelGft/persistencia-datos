package com.gft.wallet.persist.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.gft.wallet.commons.model.TransaccionDTO;
import com.gft.wallet.commons.model.TransaccionProductoClienteTipoproductoBancoDTO;
import com.gft.wallet.commons.model.TransaccionProductoTipoproductoBancoDTO;
import com.gft.wallet.persist.service.TransaccionServ;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/transaccion")
public class TransaccionController {

	@Autowired
	TransaccionServ servTrans;

	/**
	 * Busca la lista de objetos <TransaccionDTO> que se asocian a este producto
	 * especifico
	 * 
	 * @param idProducto, id del objeto <Producto> con el cual se aplicara el filtro
	 * @return flux de objetos <TransaccionDTO>, lista de transacciones asociadas
	 */
	@GetMapping("producto/{idProducto}")
	public Mono<ResponseEntity<Flux<TransaccionDTO>>> buscarProductoTodoTransaccion(@PathVariable String idProducto) {
		return Mono.just(ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
				.body(servTrans.buscarTodoTransaccionPorIdProducto(idProducto)));
	}

	/**
	 * Busca un objeto <TransaccionProductoClienteTipoproductoBancoDTO> especifico
	 * 
	 * @param id, id del objeto a buscar
	 * @return Mono de objeto <TransaccionProductoClienteTipoproductoBancoDTO>,
	 *         dentro del ResponseEntity
	 */
	@GetMapping("{id}")
	public Mono<ResponseEntity<TransaccionProductoTipoproductoBancoDTO>> buscarPorId(@PathVariable String id) {
		return servTrans.buscarPorId(id)
				.map(elem -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(elem));
	}

	/**
	 * Crea el objeto <TransaccionDTO> con los datos recibidos
	 * 
	 * @param Transaccion, objeto <TransaccionDTO> con los datos del objeto a crear
	 * @param idProducto,  id del objeto <Producto> al cual se agregara en
	 *                     asociacion
	 * @return Mono de objeto <TransaccionProductoClienteTipoproductoBancoDTO> con
	 *         los datos actualizados, dentro del ResponseEntity
	 */
	@PostMapping("producto/{idProducto}")
	public Mono<ResponseEntity<TransaccionProductoClienteTipoproductoBancoDTO>> crear(
			@Valid @RequestBody Mono<TransaccionDTO> transaccion, @PathVariable String idProducto) {
		return transaccion.flatMap(obj -> servTrans.crear(obj, idProducto)
				.map(elem -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(elem)));
	}

	/**
	 * Actualiza el objeto <TransaccionDTO> con los datos recibidos, y ademas
	 * actualiza todos los objetos que se asocian a este
	 * 
	 * @param transaccion, objeto <TransaccionDTO> con los datos a modificar
	 * @param id,          id del objeto a buscar
	 * @return Mono de objeto <TransaccionProductoClienteTipoproductoBancoDTO> con
	 *         los datos actualizados, dentro del ResponseEntity
	 */
	@PutMapping("{id}")
	public Mono<ResponseEntity<TransaccionDTO>> actualizarPorId(@Valid @RequestBody Mono<TransaccionDTO> transaccion,
			@PathVariable String id) {
		return transaccion.flatMap(obj -> servTrans.actualizar(obj, id)
				.map(elem -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(elem)));
	}

	/**
	 * Elimina un objeto <TransaccionDTO> especifico
	 * 
	 * @param id, id del objeto a eliminar
	 * @return Mono de un <ResponseEntity<Void>>
	 */
	@DeleteMapping("{id}")
	public Mono<ResponseEntity<Void>> eliminar(@PathVariable String id) {
		return servTrans.eliminar(id).then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT))
				.defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND)));
	}
}
