package com.gft.wallet.persist.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.gft.wallet.commons.model.BancoDTO;
import com.gft.wallet.commons.model.TipoproductoBancoDTO;
import com.gft.wallet.commons.model.TipoproductoDTO;
import com.gft.wallet.commons.model.listas.BancoTipoproductosDTO;
import com.gft.wallet.persist.service.BancoServ;
import com.gft.wallet.persist.service.TipoproductoServ;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/banco")
public class BancoyTipoProductoController {

	@Autowired
	BancoServ servBanco;

	@Autowired
	TipoproductoServ servTipo;

	/**
	 * Busca todos los <BancoDTO> objetos almacenados
	 * 
	 * @return Flux de objetos <BancoDTO>, dentro del ResponseEntity
	 */
	@GetMapping
	public Mono<ResponseEntity<Flux<BancoDTO>>> buscarTodoBanco() {
		return Mono.just(ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(servBanco.buscarTodos()));
	}

	/**
	 * Busca un objeto <BancoDTO> especifico
	 * 
	 * @param id, id del objeto a buscar
	 * @return Mono de objeto <BancoDTO>, dentro del ResponseEntity
	 */
	@GetMapping("{id}")
	public Mono<ResponseEntity<BancoDTO>> buscarBancoPorId(@PathVariable String id) {
		return servBanco.buscarPorId(id)
				.map(elem -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(elem));
	}

	/**
	 * Actualiza el objeto <BancoDTO> con los datos recibidos, y ademas actualiza
	 * todos los objetos que se asocian a este bano
	 * 
	 * @param banco, objeto <BancoDTO> con los datos a modificar
	 * @param id,    id del objeto a buscar
	 * @return Mono de objeto <BancoDTO> con los datos actualizados, dentro del
	 *         ResponseEntity
	 */
	@PutMapping("{id}")
	public Mono<ResponseEntity<BancoDTO>> actualizarBancoPorId(@Valid @RequestBody Mono<BancoDTO> banco,
			@PathVariable String id) {
		return banco.flatMap(obj -> servBanco.actualizar(obj, id)
				.map(elem -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(elem)));
	}

	/**
	 * Elimina un objeto <BancoDTO> especifico
	 * 
	 * @param id, id del objeto a eliminar
	 * @return Mono de un <ResponseEntity<Void>>
	 */
	@DeleteMapping("{id}")
	public Mono<ResponseEntity<Void>> eliminarBanco(@PathVariable String id) {
		return servBanco.eliminar(id).then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT))
				.defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND)));
	}

	/**
	 * Busca la lista de todos los objetos <BancoTipoproductosDTO> almacenados, que
	 * consiste en un banco y su lista de productos asociada
	 * 
	 * @return FLux de objetos <BancoTipoproductosDTO>, dentro del ResponseEntity
	 */
	@GetMapping("tipoproductos")
	public Mono<ResponseEntity<Flux<BancoTipoproductosDTO>>> buscarTodoBancoyTipoproductos() {
		return Mono.just(ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
				.body(servBanco.buscarTodoBancoyTipoproductos()));
	}

	/**
	 * Busca especificamente un objeto <BancoTipoproductosDTO>, que consiste en un
	 * banco y su lista de productos asociada
	 * 
	 * @param idBanco, id del banco a buscar
	 * @return Mono de objeto <BancoTipoproductosDTO>, dentro del ResponseEntity
	 */
	@GetMapping("tipoproductos/{idBanco}")
	public Mono<ResponseEntity<BancoTipoproductosDTO>> buscarBancoyTipoproductosPorId(@PathVariable String idBanco) {
		return servBanco.buscarBancoyTipoproductosPorId(idBanco)
				.map((elem -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(elem)));
	}

	/**
	 * Busca un objeto <TipoproductoBancoDTO> especifico, que consiste en un tipo de
	 * producto y su banco asociado
	 * 
	 * @param idTipoproducto, id
	 * @return Mono de objeto <TipoproductoBancoDTO>, dentro del ResponseEntity
	 */
	@GetMapping("tipoproducto/{idTipoproducto}")
	public Mono<ResponseEntity<TipoproductoBancoDTO>> buscarTipoproductoyBancoPorId(
			@PathVariable String idTipoproducto) {
		return servTipo.buscarTipoproductoBancoPorId(idTipoproducto)
				.map((elem -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(elem)));
	}

	/**
	 * Crea un objeto <TipoproductoDTO> con los datos del objeto recibido y el
	 * <BancoDTO> perteneciente al id recibido
	 * 
	 * @param tipoproducto, objeto <TipoproductoDTO> con los datos del objeto a
	 *                      crear
	 * @param idBanco,      id del banco al cual se agregara el <TipoproductoDTO>
	 * @return Mono de <TipoproductoBancoDTO>; en caso de no crearse regresa un
	 *         objeto nulo, dentro del ResponseEntity
	 */
	@PostMapping("{idBanco}/tipoproducto")
	public Mono<ResponseEntity<TipoproductoBancoDTO>> crearTipoproductoPorIdBanco(
			@Valid @RequestBody Mono<TipoproductoDTO> tipoproducto, @PathVariable String idBanco) {
		return tipoproducto.flatMap(obj -> servTipo.crear(obj, idBanco)
				.map(elem -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(elem)));
	}

	/**
	 * Crea un objeto <TipoproductoDTO> y <BancoDTO> con los datos del objeto
	 * recibido
	 * 
	 * @param tipoproducto, objeto <TipoproductoBancoDTO> con los datos a crear
	 * @return Mono de <TipoproductoBancoDTO>; en caso de no crearse regresa un
	 *         objeto nulo, dentro del ResponseEntity
	 */
	@PostMapping("tipoproducto")
	public Mono<ResponseEntity<TipoproductoBancoDTO>> crearBancoyTipoproducto(
			@Valid @RequestBody Mono<TipoproductoBancoDTO> tipoproducto) {
		return tipoproducto.flatMap(obj -> servTipo.crearTipoproductoyBanco(obj)
				.map(elem -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(elem)));
	}

	/**
	 * Actualiza los datos del objeto buscado por el id y se remplaza por los datos
	 * del objeto recibido
	 * 
	 * @param tipoproducto,   objeto <TipoproductoDTO> con los datos a actualizar
	 * @param idTipoproducto, cadena con el id del objeto a actualizar
	 * @return Mono de <TipoproductoBancoDTO>; en caso de no crearse regresa un
	 *         objeto nulo, dentro del ResponseEntity
	 */
	@PutMapping("tipoproducto/{idTipoproducto}")
	public Mono<ResponseEntity<TipoproductoBancoDTO>> actualizarTipoproducto(
			@RequestBody Mono<TipoproductoDTO> tipoproducto, @PathVariable String idTipoproducto) {
		return tipoproducto.flatMap(obj -> servTipo.actualizar(obj, idTipoproducto)
				.map(elem -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(elem)));
	}

	/**
	 * Elimina el objeto <TipoproductoDTO>,buscandolo por el id recibido
	 * 
	 * @param idTipoproducto, cadena con el id del objeto a eliminar
	 * @return Mono de un <ResponseEntity<Void>>
	 */
	@DeleteMapping("tipoproducto/{idTipoproducto}")
	public Mono<ResponseEntity<Void>> eliminarBancoyTipoproducto(@PathVariable String idTipoproducto) {
		return servTipo.buscarTipoproductoBancoPorId(idTipoproducto).flatMap(tipo -> {
			servBanco.eliminar(tipo.getBanco().getId()).then(servTipo.eliminar(tipo.getId()));
			return Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT))
					.defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND));
		});

	}

}
