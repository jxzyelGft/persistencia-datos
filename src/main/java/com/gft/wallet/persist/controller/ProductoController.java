package com.gft.wallet.persist.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gft.wallet.commons.model.ProductoClienteTipoproductoBancoDTO;
import com.gft.wallet.commons.model.ProductoDTO;
import com.gft.wallet.commons.model.ProductoTipoproductoBancoDTO;
import com.gft.wallet.commons.model.TransaccionDTO;
import com.gft.wallet.persist.service.ProductoServ;
import com.gft.wallet.persist.service.TransaccionServ;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/producto")
public class ProductoController {

	@Autowired
	ProductoServ servProducto;

	@Autowired
	TransaccionServ servTrans;

	/**
	 * Busca todos los objetos<ProductoTipoproductoBancoDTO> almacenados asociados
	 * aun cliente especifico
	 * 
	 * @param idCliente, cadena con el id del cliente
	 * @return Flux de objetos <ProductoTipoproductoBancoDTO>, dentro del
	 *         ResponseEntity
	 */
	@GetMapping("cliente/{idCliente}")
	public Mono<ResponseEntity<Flux<ProductoTipoproductoBancoDTO>>> buscarTodoProducto(@PathVariable String idCliente) {
		return Mono.just(ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
				.body(servProducto.buscarTodoPorCliente(idCliente)));
	}

	/**
	 * Busca un objeto <ProductoTipoproductoBancoDTO> especifico
	 * 
	 * @param id, id del objeto a buscar
	 * @return Mono de objeto <ProductoDTO>, dentro del ResponseEntity
	 */
	@GetMapping("{id}")
	public Mono<ResponseEntity<ProductoClienteTipoproductoBancoDTO>> buscarPorId(@PathVariable String id) {
		return servProducto.buscarPorId(id)
				.map(elem -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(elem));
	}

	/**
	 * Crea el objeto <ProductoTipoproductoBancoDTO> con los datos recibidos
	 * 
	 * @param producto,       objeto <ProductoDTO> con los datos del objeto a crear
	 * @param idCliente,      id del objeto <Cliente> al cual se agregara en
	 *                        asociacion
	 * @param idTipoproducto, id del objeto <Tipoproducto> al cual se agregara en
	 *                        asociacion
	 * @return Mono de objeto <ProductoDTO> con los datos actualizados, dentro del
	 *         ResponseEntity
	 */
	@PostMapping("cliente/{idCliente}/Tipoproducto/{idTipoproducto}")
	public Mono<ResponseEntity<ProductoTipoproductoBancoDTO>> crear(@Valid @RequestBody Mono<ProductoDTO> producto,
			@PathVariable String idCliente, @PathVariable String idTipoproducto) {
		return producto.flatMap(obj -> servProducto.crear(obj, idCliente, idTipoproducto)
				.map(elem -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(elem)));
	}

	/**
	 * Actualiza el objeto <ProductoDTO> con los datos recibidos, y ademas actualiza
	 * todos los objetos que se asocian a este
	 * 
	 * @param Producto, objeto <ProductoDTO> con los datos a modificar
	 * @param id,       id del objeto a buscar
	 * @return Mono de objeto <ProductoDTO> con los datos actualizados, dentro del
	 *         ResponseEntity
	 */
	@PutMapping("{id}")
	public Mono<ResponseEntity<ProductoTipoproductoBancoDTO>> actualizarPorId(
			@Valid @RequestBody Mono<ProductoDTO> producto, @PathVariable String id) {
		return producto.flatMap(obj -> servProducto.actualizar(obj, id)
				.map(elem -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(elem)));
	}

	/**
	 * Elimina un objeto <ProductoDTO> especifico
	 * 
	 * @param id, id del objeto a eliminar
	 * @return Mono de un <ResponseEntity<Void>>
	 */
	@DeleteMapping("{id}")
	public Mono<ResponseEntity<Void>> eliminar(@PathVariable String id) {
		return servProducto.eliminar(id).then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT))
				.defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND)));
	}

	/**
	 * Busca la lista de objetos <TransaccionDTO> que se asocian a este producto
	 * especifico
	 * 
	 * @param idProducto, id del objeto <Producto> con el cual se aplicara el filtro
	 * @return lista de objetos <TransaccionDTO>
	 */
	@GetMapping("{idProducto}/listaTransacciones")
	public Mono<ResponseEntity<Flux<TransaccionDTO>>> buscarTodoTransaccion(@PathVariable String idProducto) {
		return Mono.just(ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
				.body(servTrans.buscarTodoTransaccionPorIdProducto(idProducto)));
	}

}
