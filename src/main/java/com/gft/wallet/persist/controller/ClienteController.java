package com.gft.wallet.persist.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.gft.wallet.commons.model.ClienteDTO;
import com.gft.wallet.persist.service.ClienteServ;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

	@Autowired
	ClienteServ servCliente;

	/**
	 * Busca todos los <ClienteDTO> objetos almacenados
	 * 
	 * @return Flux de objetos <ClienteDTO>, dentro del ResponseEntity
	 */
	@GetMapping
	public Mono<ResponseEntity<Flux<ClienteDTO>>> buscarTodos() {
		return Mono.just(ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(servCliente.buscarTodos()));
	}

	/**
	 * Busca un objeto <ClienteDTO> especifico
	 * 
	 * @param id, id del objeto a buscar
	 * @return Mono de objeto <ClienteDTO>, dentro del ResponseEntity
	 */
	@GetMapping("{id}")
	public Mono<ResponseEntity<ClienteDTO>> buscarPorId(@PathVariable String id) {
		return servCliente.buscarPorId(id)
				.map(elem -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(elem));
	}

	/**
	 * Crea el objeto <ClienteDTO> con los datos recibidos
	 * 
	 * @param Cliente, objeto <ClienteDTO> con los datos del objeto a crear
	 * @return Mono de objeto <ClienteDTO> con los datos actualizados, dentro del
	 *         ResponseEntity
	 */
	@PostMapping()
	public Mono<ResponseEntity<ClienteDTO>> crear(@Valid @RequestBody Mono<ClienteDTO> cliente) {
		return cliente.flatMap(obj -> servCliente.crear(obj)
				.map(elem -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(elem)));
	}

	/**
	 * Actualiza el objeto <ClienteDTO> con los datos recibidos, y ademas actualiza
	 * todos los objetos que se asocian a este
	 * 
	 * @param Cliente, objeto <ClienteDTO> con los datos a modificar
	 * @param id,      id del objeto a buscar
	 * @return Mono de objeto <ClienteDTO> con los datos actualizados, dentro del
	 *         ResponseEntity
	 */
	@PutMapping("{id}")
	public Mono<ResponseEntity<ClienteDTO>> actualizarPorId(@Valid @RequestBody Mono<ClienteDTO> cliente,
			@PathVariable String id) {
		return cliente.flatMap(obj -> servCliente.actualizar(obj, id)
				.map(elem -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(elem)));
	}

	/**
	 * Elimina un objeto <ClienteDTO> especifico
	 * 
	 * @param id, id del objeto a eliminar
	 * @return Mono de un <ResponseEntity<Void>>
	 */
	@DeleteMapping("{id}")
	public Mono<ResponseEntity<Void>> eliminar(@PathVariable String id) {
		return servCliente.eliminar(id).then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT))
				.defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND)));
	}

	/**
	 * Crea el objeto <ClienteDTO> con los datos recibidos
	 * 
	 * @param Cliente, objeto <ClienteDTO> con los datos del objeto a crear
	 * @return Mono de objeto <ClienteDTO> con los datos actualizados, dentro del
	 *         ResponseEntity
	 */
	@PostMapping("/porCorreo")
	public Mono<ResponseEntity<ClienteDTO>> IniciarSesion(@Valid @RequestBody Mono<ClienteDTO> cliente) {
		return cliente.flatMap(obj -> servCliente.buscarPorCorreo(obj.getCorreo())
				.map(elem -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(elem)));
	}

}
