package com.gft.wallet.persist.dao;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import com.gft.wallet.persist.models.Banco;

public interface BancoDao extends ReactiveMongoRepository<Banco, String> {

}
//{categorias : { $elemMatch : {nombre : "Deporte"}}}