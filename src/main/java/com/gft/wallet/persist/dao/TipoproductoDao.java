package com.gft.wallet.persist.dao;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import com.gft.wallet.persist.models.Tipoproducto;
import reactor.core.publisher.Flux;

public interface TipoproductoDao extends ReactiveMongoRepository<Tipoproducto, String> {

//	$elemMatch
	@Query("{ 'banco._id': ?0}")
	Flux<Tipoproducto> findByBanco(String idBanco);

}
