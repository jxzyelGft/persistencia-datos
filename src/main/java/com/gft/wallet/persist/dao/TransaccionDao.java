package com.gft.wallet.persist.dao;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import com.gft.wallet.persist.models.Transaccion;
import reactor.core.publisher.Flux;

public interface TransaccionDao extends ReactiveMongoRepository<Transaccion, String> {
	@Query("{ 'producto._id': ?0}")
	Flux<Transaccion> findByProducto(String idProducto);

	@Query(value = "{ 'producto._id': ?0}", fields = "{ id : 1}")
	Flux<String> findIdsByProducto(String idProducto);
}
