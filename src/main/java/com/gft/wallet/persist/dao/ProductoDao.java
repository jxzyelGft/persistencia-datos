package com.gft.wallet.persist.dao;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import com.gft.wallet.persist.models.Producto;
import reactor.core.publisher.Flux;

public interface ProductoDao extends ReactiveMongoRepository<Producto, String> {

	@Query("{ 'Cliente._id': ?0}")
	Flux<Producto> findByCliente(String idCliente);

	@Query(value = "{ 'Tipoproducto._id': ?0}", fields = "{ id : 1}")
	Flux<String> findIdsByTipoproducto(String idTipoproducto);

	@Query(value = "{ 'Cliente._id': ?0}", fields = "{ id : 1}")
	Flux<String> findIdsByCliente(String idCliente);
}
