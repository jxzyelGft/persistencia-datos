package com.gft.wallet.persist.dao;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import com.gft.wallet.persist.models.Cliente;
import reactor.core.publisher.Mono;

public interface ClienteDao extends ReactiveMongoRepository<Cliente, String> {

	@Query("{ 'correo': ?0, 'clave' : ?1}")
	Mono<Cliente> findByCorreoyClave(String correo, String clave);
	
	@Query("{ 'correo': ?0}")
	Mono<Cliente> findByCorreo(String correo);
}
