package com.gft.wallet.persist.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gft.wallet.commons.model.TransaccionDTO;
import com.gft.wallet.commons.model.TransaccionProductoClienteTipoproductoBancoDTO;
import com.gft.wallet.commons.model.TransaccionProductoTipoproductoBancoDTO;
import com.gft.wallet.commons.model.listas.ProductoTipoproductoBancoTransaccionesDTO;
import com.gft.wallet.persist.dao.TransaccionDao;
import com.gft.wallet.persist.mapper.ProductoMapper;
import com.gft.wallet.persist.mapper.TransaccionMapper;
import com.gft.wallet.persist.models.Producto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class TransaccionServImpl implements TransaccionServ {

	@Autowired
	TransaccionDao dao;

	@Autowired
	ProductoServ servProd;

	@Override
	public Mono<TransaccionProductoTipoproductoBancoDTO> buscarPorId(String idTransaccion) {
		return dao.findById(idTransaccion)
				.map(trans -> TransaccionMapper.INSTANCE.toTransaccionProductoTipoproductoBancoDTO(trans));
	}

	@Override
	public Flux<String> buscarTodosIdTransaccionPorIdProducto(String idProducto) {
		return dao.findIdsByProducto(idProducto).map(elem -> elem.substring(9, elem.length() - 2));
	}

	@Override
	public Mono<ProductoTipoproductoBancoTransaccionesDTO> buscarProductoCompuestoyTransaccionesPorProducto(
			String idProducto) {
		return servProd.buscarPorId(idProducto)
				.flatMap(prod -> dao.findByProducto(idProducto)
						.map(trans -> TransaccionMapper.INSTANCE.toTransaccionDTO(trans)).collectList()
						.map(lista -> new ProductoTipoproductoBancoTransaccionesDTO(
								ProductoMapper.INSTANCE.toProductoTipoproductoBancoDTO(prod), lista)));
	}

	@Override
	public Mono<TransaccionProductoClienteTipoproductoBancoDTO> crear(TransaccionDTO transaccion, String idProducto) {
		return servProd.buscarPorId(idProducto)
				.flatMap(prod -> dao.save(TransaccionMapper.INSTANCE.toTransaccion(transaccion, prod))
						.map(nvotrans -> TransaccionMapper.INSTANCE
								.toTransaccionProductoClienteTipoproductoBancoDTO(nvotrans)));
	}

	@Override
	public Mono<TransaccionProductoClienteTipoproductoBancoDTO> actualizar(TransaccionDTO transaccion,
			String idTransaccion) {
		return dao.findById(idTransaccion).flatMap(objExistente -> {
			if (objExistente == null) {
				return Mono.just(null);
			} else {
				return dao.save(TransaccionMapper.INSTANCE.toTransaccionSimple(transaccion))
						.map(nvotrans -> TransaccionMapper.INSTANCE
								.toTransaccionProductoClienteTipoproductoBancoDTO(nvotrans));
			}
		});
	}

	@Override
	public Mono<TransaccionDTO> actualizarProductoHeredado(Producto producto, String idTransaccion) {
		return dao.findById(idTransaccion).flatMap(trans -> {
			trans.setProducto(producto);
			return dao.save(trans).map(nvotrans -> TransaccionMapper.INSTANCE.toTransaccionDTO(nvotrans));
		});
	}

	@Override
	public Mono<Void> eliminar(String id) {
		return dao.deleteById(id);
	}

	@Override
	public Flux<TransaccionDTO> buscarTodoTransaccionPorIdProducto(String idProducto) {
		return dao.findByProducto(idProducto).map(elem -> TransaccionMapper.INSTANCE.toTransaccionDTO(elem));
	}

}
