package com.gft.wallet.persist.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gft.wallet.commons.model.TipoproductoBancoDTO;
import com.gft.wallet.commons.model.TipoproductoDTO;
import com.gft.wallet.persist.dao.TipoproductoDao;
import com.gft.wallet.persist.mapper.SimpleEntityMapper;
import com.gft.wallet.persist.mapper.TipoproductoMapper;
import com.gft.wallet.persist.models.Banco;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class TipoproductoServImpl implements TipoproductoServ {

	@Autowired
	TipoproductoDao dao;

	@Autowired
	BancoServ serBan;

	@Autowired
	ProductoServ servPro;

	@Override
	public Mono<TipoproductoDTO> buscarPorId(String id) {
		return dao.findById(id).map(elem -> TipoproductoMapper.INSTANCE.toTipoproductoDTO(elem));
	}

	@Override
	public Flux<TipoproductoDTO> buscarTodos() {
		return dao.findAll().map(elem -> TipoproductoMapper.INSTANCE.toTipoproductoDTO(elem));
	}

	@Override
	public Mono<TipoproductoBancoDTO> crearTipoproductoyBanco(TipoproductoBancoDTO tipoproducto) {
		return serBan.crear(tipoproducto.getBanco()).flatMap(banco -> {
			tipoproducto.setId(null);
			return dao.save(TipoproductoMapper.INSTANCE.toTipoproducto(tipoproducto))
					.map(elem -> TipoproductoMapper.INSTANCE.toTipoproductoBancoDTO(elem));
		});
	}

	@Override
	public Mono<TipoproductoBancoDTO> crear(TipoproductoDTO tipoproducto, String idBanco) {
		return serBan.buscarPorId(idBanco).flatMap(banco -> {
			return dao
					.save(TipoproductoMapper.INSTANCE.toTipoproducto(tipoproducto,
							SimpleEntityMapper.INSTANCE.bancoDTOToBanco(banco)))
					.map(elem -> TipoproductoMapper.INSTANCE.toTipoproductoBancoDTO(elem));
		});
	}

	@Override
	public Mono<TipoproductoBancoDTO> actualizar(TipoproductoDTO tipoproducto, String id) {
		return dao.findById(id).flatMap(tipo -> {
			tipo.setNombre(tipoproducto.getNombre());
			tipo.setTipo(tipoproducto.getTipo());
			return dao.save(tipo).map(elem -> {
				return TipoproductoMapper.INSTANCE.toTipoproductoBancoDTO(elem);
			});
		});
	}

	@Override
	public Mono<TipoproductoDTO> actualizarBancoHeredado(Banco banco, String idTipoproducto) {
		return dao.findById(idTipoproducto).flatMap(elem -> {
			elem.getBanco().setNombre(banco.getNombre());
			elem.getBanco().setImg(banco.getImg());
			servPro.buscarTodosIdProductoPorIdTipoproducto(elem.getId())
					.flatMap(ids -> servPro.actualizarTipoproductoHeredado(elem, ids)).subscribe();
			return dao.save(elem).map(obj -> TipoproductoMapper.INSTANCE.toTipoproductoDTO(obj));
		});
	}

	@Override
	public Mono<Void> eliminar(String id) {
		return dao.deleteById(id);
	}

	@Override
	public Flux<TipoproductoDTO> buscarTodoPorBanco(String idBanco) {
		return dao.findByBanco(idBanco).map(elem -> TipoproductoMapper.INSTANCE.toTipoproductoDTO(elem));
	}

	@Override
	public Mono<TipoproductoBancoDTO> buscarTipoproductoBancoPorId(String id) {
		return dao.findById(id).map(elem -> TipoproductoMapper.INSTANCE.toTipoproductoBancoDTO(elem));
	}

}
