package com.gft.wallet.persist.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gft.wallet.commons.model.BancoDTO;
import com.gft.wallet.commons.model.listas.BancoTipoproductosDTO;
import com.gft.wallet.persist.dao.BancoDao;
import com.gft.wallet.persist.mapper.SimpleEntityMapper;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
//@Transactional
public class BancoServImpl implements BancoServ {

	@Autowired
	BancoDao dao;

	@Autowired
	TipoproductoServ servTipo;

	@Override
	public Mono<BancoDTO> buscarPorId(String id) {
		return dao.findById(id).flatMap(elem -> Mono.just(SimpleEntityMapper.INSTANCE.bancoToBancoDTO(elem)));
	}

	@Override
	public Mono<BancoTipoproductosDTO> buscarBancoyTipoproductosPorId(String idBanco) {
		return dao.findById(idBanco).flatMap(elem -> servTipo.buscarTodoPorBanco(elem.getId()).collectList()
				.map(list -> new BancoTipoproductosDTO(idBanco, elem.getNombre(), elem.getImg(), list)));
	}

	@Override
	public Flux<BancoDTO> buscarTodos() {
		return dao.findAll().map(elem -> SimpleEntityMapper.INSTANCE.bancoToBancoDTO(elem));
	}

	@Override
	public Flux<BancoTipoproductosDTO> buscarTodoBancoyTipoproductos() {
		return dao.findAll().flatMap(elem -> servTipo.buscarTodoPorBanco(elem.getId()).collectList()
				.map(list -> new BancoTipoproductosDTO(elem.getId(), elem.getNombre(), elem.getImg(), list)));
	}

	@Override
	public Mono<BancoDTO> crear(BancoDTO banco) {
		return dao.save(SimpleEntityMapper.INSTANCE.bancoDTOToBanco(banco))
				.map(elem -> SimpleEntityMapper.INSTANCE.bancoToBancoDTO(elem));
	}

	@Override
	public Mono<BancoDTO> actualizar(BancoDTO banco, String id) {
		return dao.findById(id).flatMap(objExistente -> {
			if (objExistente == null) {
				return Mono.just(null);
			} else {
				return dao.save(SimpleEntityMapper.INSTANCE.bancoDTOToBanco(banco)).map(obj -> {
					servTipo.buscarTodoPorBanco(banco.getId())
							.map(elem -> servTipo.actualizarBancoHeredado(obj, elem.getId())).subscribe();
					return SimpleEntityMapper.INSTANCE.bancoToBancoDTO(obj);
				});
			}
		});
	}

	@Override
	public Mono<Void> eliminar(String id) {
		return dao.findById(id).flatMap(elem -> dao.deleteById(elem.getId()));
	}

}
