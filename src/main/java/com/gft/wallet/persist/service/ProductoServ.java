package com.gft.wallet.persist.service;

import com.gft.wallet.commons.model.ProductoClienteTipoproductoBancoDTO;
import com.gft.wallet.commons.model.ProductoDTO;
import com.gft.wallet.commons.model.ProductoTipoproductoBancoDTO;
import com.gft.wallet.persist.models.Cliente;
import com.gft.wallet.persist.models.Tipoproducto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ProductoServ {

	/**
	 * Retorna el objeto <ProductoClienteTipoproductoBancoDTO>, que es un objeto
	 * <ProductoDTO> y su objeto asociado <TipoproductoBancoDTO>
	 * 
	 * @param idProducto, cadena con el id del <ProductoDTO>
	 * @return Mono de <ProductoClienteTipoproductoBancoDTO>
	 */
	Mono<ProductoClienteTipoproductoBancoDTO> buscarPorId(String idProducto);

	/**
	 * Retorna la lista de id's de todos los objetos <ProductoDTO> que se asocian
	 * con los parametros recibidos
	 * 
	 * @param idCliente, id del cliente del cual buscar los id's
	 * 
	 * @return Flux de id's <String>
	 */
	Flux<String> buscarTodosIdProductoPorIdCliente(String idCliente);

	/**
	 * Retorna la lista de id's de todos los objetos <ProductoDTO> que se asocian
	 * con los parametros recibidos
	 * 
	 * @param idTipoproducto, id del tipo de producto del cual buscar los id's
	 * 
	 * @return Flux de id's <String>
	 */
	Flux<String> buscarTodosIdProductoPorIdTipoproducto(String idTipoproducto);

	/**
	 * Retorna la lista de todos los objetos <ProductoTipoproductoBancoDTO> que
	 * pertenecen a un cliente especifico
	 * 
	 * @param idBanco, id del objeto <Cliente> con el cual se aplicara el filtro
	 * @return Flux de objetos filtrados <ProductoTipoproductoBancoDTO>
	 */
	Flux<ProductoTipoproductoBancoDTO> buscarTodoPorCliente(String idCliente);

	/**
	 * Crea un objeto <ProductoDTO> con los datos recibidos en el objeto y los id de
	 * las asociaciones recibidas
	 * 
	 * @param Producto,       objeto <ProductoDTO> con los datos a guardar
	 * @param idCliente,      id del cliente al que va asociado el objeto
	 * @param idTipoproducto, id del tipos de producto al que va asociado el objeto
	 * @return Mono de <ProductoDTO>; en caso de no crearse regresa un objeto nulo
	 */
	Mono<ProductoTipoproductoBancoDTO> crear(ProductoDTO Producto, String idCliente, String idTipoproducto);

	/**
	 * Actualiza los datos del objeto buscado por el id y se remplaza por los datos
	 * del objeto recibido
	 * 
	 * @param producto,   objeto <ProductoDTO> con los datos a guardar
	 * @param idProducto, id del objeto producto a modificar
	 * @return Mono de <ProductoDTO>; en caso de no modificarse regresa un objeto
	 *         nulo
	 */
	Mono<ProductoTipoproductoBancoDTO> actualizar(ProductoDTO producto, String idProducto);

	/**
	 * Actualiza los datos del objeto heredado buscando por el id y se remplaza por
	 * los datos del objeto recibido
	 * 
	 * @param tipoproducto, objeto <Tipoproducto> con los datos a actualizar el
	 *                      objeto heredado
	 * @param idProducto,   cadena con el id del objeto a actualizar
	 * @return Mono de objeto <ProductoDTO>
	 */
	Mono<ProductoDTO> actualizarTipoproductoHeredado(Tipoproducto tipoproducto, String idProducto);

	/**
	 * Actualiza los datos del objeto heredado buscando por el id y se remplaza por
	 * los datos del objeto recibido
	 * 
	 * @param cliente,    objeto <Cliente> con los datos a actualizar el objeto
	 *                    heredado
	 * @param idProducto, cadena con el id del objeto a actualizar
	 * @return Mono de objeto <ProductoDTO>
	 */
	Mono<ProductoDTO> actualizarClienteHeredado(Cliente cliente, String idProducto);

	/**
	 * Elimina el objeto <ProductoDTO> buscandolo por el id recibido
	 * 
	 * @param id, cadena con el id del objeto a eliminar
	 */
	Mono<Void> eliminar(String id);

}