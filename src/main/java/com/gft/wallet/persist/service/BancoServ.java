package com.gft.wallet.persist.service;

import com.gft.wallet.commons.model.BancoDTO;
import com.gft.wallet.commons.model.listas.BancoTipoproductosDTO;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface BancoServ {

	/**
	 * Retorna el objeto <BancoDTO>
	 * 
	 * @param id, cadena con el id del <BancoDTO>
	 * @return Mono de <BancoDTO>
	 */
	Mono<BancoDTO> buscarPorId(String id);

	/**
	 * Retorna el objeto <BancoDTO> y una lista de <Tipoproducto> asociada al mismo
	 * 
	 * @param idBanco, cadena con el id del <BancoDTO>
	 * @return Mono de <BancoDTO> y su lista asociada
	 */
	Mono<BancoTipoproductosDTO> buscarBancoyTipoproductosPorId(String idBanco);

	/**
	 * Retorna la lista de todos los objetos <BancoDTO>
	 * 
	 * @return Flux de objetos <BancoDTO>
	 */
	Flux<BancoDTO> buscarTodos();

	/**
	 * Retorna la lista de todos los objetos <BancoDTO> y la lista de <Tipoproducto>
	 * asociada a cada uno
	 * 
	 * @return Flux de objetos <BancoDTO> y su lista asociada
	 */
	Flux<BancoTipoproductosDTO> buscarTodoBancoyTipoproductos();

	/**
	 * Crea un objeto <BancoDTO>
	 * 
	 * @param banco, objeto <BancoDTO>
	 * @return Mono de <BancoDTO>; en caso de no crearse regresa un objeto nulo
	 */
	Mono<BancoDTO> crear(BancoDTO banco);

	/**
	 * Actualiza los datos del objeto buscado por el id y se remplaza por los datos
	 * del objeto recibido
	 * 
	 * @param banco, objeto <BancoDTO>
	 * @param id,    cadena con el id del objeto a actualizar
	 * @return Mono de <BancoDTO>; en caso de no modificarse regresa un objeto nulo
	 */
	Mono<BancoDTO> actualizar(BancoDTO banco, String id);

	/**
	 * Elimina el objeto <BancoDTO> y la lista de <Tipoproducto> asociada al mismo,
	 * buscandolo por el id recibido
	 * 
	 * @param id, cadena con el id del objeto a eliminar
	 */
	Mono<Void> eliminar(String id);

}