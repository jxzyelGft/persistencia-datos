package com.gft.wallet.persist.service;

import com.gft.wallet.commons.model.TransaccionDTO;
import com.gft.wallet.commons.model.TransaccionProductoClienteTipoproductoBancoDTO;
import com.gft.wallet.commons.model.TransaccionProductoTipoproductoBancoDTO;
import com.gft.wallet.commons.model.listas.ProductoTipoproductoBancoTransaccionesDTO;
import com.gft.wallet.persist.models.Producto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface TransaccionServ {

	/**
	 * Retorna el objeto <TransaccionProductoClienteTipoproductoBancoDTO>, que es un
	 * objeto <ProductoDTO> y su objeto asociado <TipoproductoBancoDTO>
	 * 
	 * @param idTransaccion, cadena con el id del <TransaccionDTO>
	 * @return Mono de <TransaccionProductoClienteTipoproductoBancoDTO>
	 */
	Mono<TransaccionProductoTipoproductoBancoDTO> buscarPorId(String idTransaccion);

	/**
	 * Retorna la lista de id's de todos los objetos <TransaccionDTO> que se asocian
	 * con los parametros recibidos
	 * 
	 * @param idProducto, id del productos del cual buscar los id's
	 * @return Flux de id's <String>
	 */
	Flux<String> buscarTodosIdTransaccionPorIdProducto(String idProducto);

	/**
	 * Retorna todos los objetos <TransaccionDTO> que se asocian con los parametros
	 * recibidos
	 * 
	 * @param idProducto, id del productos del cual buscar los id's
	 * @return Flux de id's <TransaccionDTO>
	 */
	Flux<TransaccionDTO> buscarTodoTransaccionPorIdProducto(String idProducto);

	/**
	 * Retorna el objeto <ProductoTipoproductoBanco> y la lista de todos los objetos
	 * <TransaccionDTO> que pertenecen a este producto especifico
	 * 
	 * @param idProducto, id del objeto <Producto> con el cual se aplicara el filtro
	 * @return objeto <ProductoTipoproductoBancoTransaccionesDTO>, compuesto con el
	 *         producto y la lista de transacciones asociadas
	 */
	Mono<ProductoTipoproductoBancoTransaccionesDTO> buscarProductoCompuestoyTransaccionesPorProducto(String idProducto);

	/**
	 * Crea un objeto <TransaccionDTO> con los datos recibidos en el objeto y el id
	 * de asociacion recibido
	 * 
	 * @param transaccion, objeto <TransaccionDTO> con los datos a guardar
	 * @param idProducto,  id del objeto <Producto> al que va asociado el objeto
	 * @return Mono de <TransaccionProductoClienteTipoproductoBancoDTO>; en caso de
	 *         no crearse regresa un objeto nulo
	 */
	Mono<TransaccionProductoClienteTipoproductoBancoDTO> crear(TransaccionDTO transaccion, String idProducto);

	/**
	 * Actualiza los datos del objeto buscado por el id y se remplaza por los datos
	 * del objeto recibido
	 * 
	 * @param transaccion,   objeto <TransaccionDTO> con los datos a guardar
	 * @param idTransaccion, id del objeto transaccion a modificar
	 * @return Mono de <TransaccionProductoClienteTipoproductoBancoDTO>; en caso de
	 *         no modificarse regresa un objeto nulo
	 */
	Mono<TransaccionProductoClienteTipoproductoBancoDTO> actualizar(TransaccionDTO transaccion, String idTransaccion);

	/**
	 * Actualiza los datos del objeto heredado buscando por el id y se remplaza por
	 * los datos del objeto recibido
	 * 
	 * @param producto,      objeto <Producto> con los datos a actualizar el objeto
	 *                       heredado
	 * @param idTransaccion, cadena con el id del objeto a actualizar
	 * @return Mono de objeto <TransaccionDTO>
	 * 
	 */
	Mono<TransaccionDTO> actualizarProductoHeredado(Producto producto, String idTransaccion);

	/**
	 * Elimina el objeto <TransaccionDTO> buscandolo por el id recibido
	 * 
	 * @param id, cadena con el id del objeto a eliminar
	 */
	Mono<Void> eliminar(String id);

}