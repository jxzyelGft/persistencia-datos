package com.gft.wallet.persist.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import com.gft.wallet.commons.model.TipoproductoBancoDTO;
import com.gft.wallet.commons.model.TipoproductoDTO;
import com.gft.wallet.persist.models.Banco;

public interface TipoproductoServ {

	/**
	 * Retorna el objeto <TipoproductoDTO> buscandolo por el id recibido
	 * 
	 * @param id, cadena con el id del <TipoproductoDTO>
	 * @return Mono de <TipoproductoDTO>
	 */
	Mono<TipoproductoDTO> buscarPorId(String id);

	/**
	 * Retorna el objeto <TipoproductoBancoDTO> buscandolo por el id recibido
	 * 
	 * @param id, cadena con el id del <TipoproductoDTO>
	 * @return Mono de <TipoproductoBanco>
	 */
	Mono<TipoproductoBancoDTO> buscarTipoproductoBancoPorId(String id);

	/**
	 * Retorna la lista de todos los objetos <TipoproductoDTO>
	 * 
	 * @return Flux de objetos <TipoproductoDTO>
	 */
	Flux<TipoproductoDTO> buscarTodos();

	/**
	 * Retorna la lista de todos los objetos <TipoproductoDTO> que pertenecen a un
	 * banco especifico
	 * 
	 * @param idBanco, id del objeto <Banco> con el cual se aplicara el filtro
	 * @return Flux de objetos filtrados <TipoproductoDTO>
	 */
	Flux<TipoproductoDTO> buscarTodoPorBanco(String idBanco);

	/**
	 * Crea un objeto <TipoproductoDTO> con los datos del objeto recibido y el
	 * <BancoDTO> perteneciente al id recibido
	 * 
	 * @param tipoproducto, objeto <TipoproductoDTO> con los datos a crear
	 * @param idBanco,      id del banco al cual se agregara el <TipoproductoDTO>
	 * @return Mono de <TipoproductoBancoDTO>; en caso de no crearse regresa un
	 *         objeto nulo
	 */
	Mono<TipoproductoBancoDTO> crear(TipoproductoDTO tipoproducto, String idBanco);

	/**
	 * Crea un objeto <TipoproductoDTO> y <BancoDTO> con los datos del objeto
	 * recibido
	 * 
	 * @param tipoproducto, objeto <TipoproductoBancoDTO> con los datos a crear
	 * @return Mono de <TipoproductoBancoDTO>; en caso de no crearse regresa un
	 *         objeto nulo
	 */
	Mono<TipoproductoBancoDTO> crearTipoproductoyBanco(TipoproductoBancoDTO tipoproducto);

	/**
	 * Actualiza los datos del objeto heredado buscando por el id y se remplaza por
	 * los datos del objeto recibido
	 * 
	 * @param banco,          objeto <Banco> con los datos a actualizar el objeto
	 *                        heredado
	 * @param idTipoproducto, cadena con el id del objeto a actualizar
	 * @return Mono de objeto <TipoproductoDTO>
	 * 
	 */
	Mono<TipoproductoDTO> actualizarBancoHeredado(Banco banco, String idTipoproducto);

	/**
	 * Actualiza los datos del objeto buscado por el id y se remplaza por los datos
	 * del objeto recibido
	 * 
	 * @param tipoproducto, objeto <TipoproductoDTO> con los datos a actualizar
	 * @param id,           cadena con el id del objeto a actualizar
	 * @return Mono de <TipoproductoBancoDTO>; en caso de no modificarse regresa un
	 *         objeto nulo
	 */
	Mono<TipoproductoBancoDTO> actualizar(TipoproductoDTO tipoproducto, String id);

	/**
	 * Elimina el objeto <TipoproductoDTO>,buscandolo por el id recibido
	 * 
	 * @param id, cadena con el id del objeto a eliminar
	 */
	Mono<Void> eliminar(String id);
}
