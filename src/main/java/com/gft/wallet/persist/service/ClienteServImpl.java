package com.gft.wallet.persist.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gft.wallet.commons.model.ClienteDTO;
import com.gft.wallet.persist.dao.ClienteDao;
import com.gft.wallet.persist.mapper.SimpleEntityMapper;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ClienteServImpl implements ClienteServ {

	@Autowired
	ClienteDao dao;

	@Autowired
	ProductoServ servPro;

	@Override
	public Mono<ClienteDTO> buscarPorId(String id) {
		return dao.findById(id).map(elem -> SimpleEntityMapper.INSTANCE.clienteToClienteDTO(elem));
	}

	@Override
	public Flux<ClienteDTO> buscarTodos() {
		return dao.findAll().map(elem -> SimpleEntityMapper.INSTANCE.clienteToClienteDTO(elem));
	}

	@Override
	public Mono<ClienteDTO> crear(ClienteDTO cliente) {
		return dao.save(SimpleEntityMapper.INSTANCE.clienteDTOToCliente(cliente))
				.map(elem -> SimpleEntityMapper.INSTANCE.clienteToClienteDTO(elem));
	}

	@Override
	public Mono<ClienteDTO> actualizar(ClienteDTO cliente, String id) {
		return dao.findById(id).flatMap(objExistente -> {
			if (objExistente == null) {
				return Mono.just(null);
			} else {
				return dao.save(SimpleEntityMapper.INSTANCE.clienteDTOToCliente(cliente)).map(cli -> {
					servPro.buscarTodosIdProductoPorIdCliente(cli.getId())
							.flatMap(ids -> servPro.actualizarClienteHeredado(cli, ids)).subscribe();
					return SimpleEntityMapper.INSTANCE.clienteToClienteDTO(cli);
				});
			}
		});
	}

	@Override
	public Mono<Void> eliminar(String id) {
		return dao.deleteById(id);
	}

	@Override
	public Mono<ClienteDTO> buscarPorCorreoyClave(String correo, String clave) {
		return dao.findByCorreoyClave(correo, clave).map(cli -> SimpleEntityMapper.INSTANCE.clienteToClienteDTO(cli));
	}

	@Override
	public Mono<ClienteDTO> buscarPorCorreo(String correo) {
		return dao.findByCorreo(correo).map(cli -> SimpleEntityMapper.INSTANCE.clienteToClienteDTO(cli));
	}
}
