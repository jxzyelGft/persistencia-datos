package com.gft.wallet.persist.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gft.wallet.commons.model.ProductoClienteTipoproductoBancoDTO;
import com.gft.wallet.commons.model.ProductoDTO;
import com.gft.wallet.commons.model.ProductoTipoproductoBancoDTO;
import com.gft.wallet.persist.dao.ProductoDao;
import com.gft.wallet.persist.mapper.ProductoMapper;
import com.gft.wallet.persist.models.Cliente;
import com.gft.wallet.persist.models.Tipoproducto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ProductoServImpl implements ProductoServ {

	@Autowired
	ProductoDao dao;

	@Autowired
	TipoproductoServ servTipo;

	@Autowired
	ClienteServ servCli;

	@Override
	public Mono<ProductoClienteTipoproductoBancoDTO> buscarPorId(String idProducto) {
		return dao.findById(idProducto)
				.map(prod -> ProductoMapper.INSTANCE.toProductoClienteTipoproductoBancoDTO(prod));
	}

	@Override
	public Flux<String> buscarTodosIdProductoPorIdCliente(String idCliente) {
		return dao.findIdsByCliente(idCliente).map(elem -> elem.substring(9, elem.length() - 2));
	}

	@Override
	public Flux<String> buscarTodosIdProductoPorIdTipoproducto(String idTipoproducto) {
		return dao.findIdsByTipoproducto(idTipoproducto).map(elem -> elem.substring(9, elem.length() - 2));
	}

	@Override
	public Flux<ProductoTipoproductoBancoDTO> buscarTodoPorCliente(String idCliente) {
		return dao.findByCliente(idCliente).map(prod -> ProductoMapper.INSTANCE.toProductoTipoproductoBancoDTO(prod));
	}

	@Override
	public Mono<ProductoTipoproductoBancoDTO> crear(ProductoDTO producto, String idCliente, String idTipoproducto) {
		return servCli.buscarPorId(idCliente)
				.flatMap(cli -> servTipo.buscarTipoproductoBancoPorId(idTipoproducto)
						.flatMap(tipo -> dao.save(ProductoMapper.INSTANCE.toProducto(producto, tipo, cli))
								.map(obj -> ProductoMapper.INSTANCE.toProductoTipoproductoBancoDTO(obj))));
	}

	@Override
	public Mono<ProductoTipoproductoBancoDTO> actualizar(ProductoDTO producto, String idProducto) {
		return dao.findById(idProducto).flatMap(objExistente -> {
			if (objExistente == null) {
				return Mono.just(null);
			} else {
				objExistente.setAlias(producto.getAlias());
				objExistente.setEstado(producto.getEstado());
				objExistente.setCcv(producto.getCcv());
				objExistente.setExpiracion(producto.getExpiracion());
				return dao.save(objExistente)
						.map(nvoprod -> ProductoMapper.INSTANCE.toProductoTipoproductoBancoDTO(nvoprod));
			}
		});
	}

	@Override
	public Mono<ProductoDTO> actualizarTipoproductoHeredado(Tipoproducto tipoproducto, String idProducto) {
		return dao.findById(idProducto).flatMap(prod -> {
			prod.getTipoproducto().setBanco(tipoproducto.getBanco());
			prod.getTipoproducto().setNombre(tipoproducto.getNombre());
			prod.getTipoproducto().setTipo(tipoproducto.getTipo());
			return dao.save(prod).map(nvoprod -> ProductoMapper.INSTANCE.toProductoDTO(nvoprod));
		});
	}

	@Override
	public Mono<ProductoDTO> actualizarClienteHeredado(Cliente cliente, String idProducto) {
		return dao.findById(idProducto).flatMap(prod -> {
			prod.getCliente().setNombreCompleto(cliente.getNombreCompleto());
			prod.getCliente().setCorreo(cliente.getCorreo());
			prod.getCliente().setClave(cliente.getClave());
			return dao.save(prod).map(nvoprod -> ProductoMapper.INSTANCE.toProductoDTO(nvoprod));
		});
	}

	@Override
	public Mono<Void> eliminar(String id) {
		return dao.deleteById(id);
	}

}
