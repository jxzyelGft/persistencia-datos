package com.gft.wallet.persist.service;

import com.gft.wallet.commons.model.ClienteDTO;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ClienteServ {

	/**
	 * Retorna el objeto <ClienteDTO>
	 * 
	 * @param id, cadena con el id del <ClienteDTO>
	 * @return Mono de <ClienteDTO>
	 */
	Mono<ClienteDTO> buscarPorId(String id);

	/**
	 * Retorna el objeto <ClienteDTO>
	 * 
	 * @param correo, correo del <ClienteDTO>
	 * @param clave,  clave del <ClienteDTO>
	 * @return Mono de <ClienteDTO>
	 */
	Mono<ClienteDTO> buscarPorCorreoyClave(String correo, String clave);

	/**
	 * Retorna el objeto <ClienteDTO>
	 * 
	 * @param correo, correo del <ClienteDTO>
	 * @return Mono de <ClienteDTO>
	 */
	Mono<ClienteDTO> buscarPorCorreo(String correo);

	/**
	 * Retorna la lista de todos los objetos <ClienteDTO>
	 * 
	 * @return Flux de objetos <ClienteDTO>
	 */
	Flux<ClienteDTO> buscarTodos();

	/**
	 * Crea un objeto <ClienteDTO>
	 * 
	 * @param Cliente, objeto <ClienteDTO>
	 * @return Mono de <ClienteDTO>; en caso de no crearse regresa un objeto nulo
	 */
	Mono<ClienteDTO> crear(ClienteDTO Cliente);

	/**
	 * Actualiza los datos del objeto buscado por el id y se remplaza por los datos
	 * del objeto recibido
	 * 
	 * @param Cliente, objeto <ClienteDTO>
	 * @param id,      cadena con el id del objeto a actualizar
	 * @return Mono de <ClienteDTO>; en caso de no modificarse regresa un objeto
	 *         nulo
	 */
	Mono<ClienteDTO> actualizar(ClienteDTO Cliente, String id);

	/**
	 * Elimina el objeto <ClienteDTO> y la lista de <Tipoproducto> asociada al
	 * mismo, buscandolo por el id recibido
	 * 
	 * @param id, cadena con el id del objeto a eliminar
	 */
	Mono<Void> eliminar(String id);

}