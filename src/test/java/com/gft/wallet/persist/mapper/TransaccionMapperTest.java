package com.gft.wallet.persist.mapper;

import static org.junit.Assert.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.junit.Test;
import com.gft.wallet.commons.model.ProductoClienteTipoproductoBancoDTO;
import com.gft.wallet.commons.model.ProductoTipoproductoBancoDTO;
import com.gft.wallet.commons.model.TransaccionDTO;
import com.gft.wallet.commons.model.TransaccionProductoClienteTipoproductoBancoDTO;
import com.gft.wallet.commons.model.TransaccionProductoTipoproductoBancoDTO;
import com.gft.wallet.persist.models.Banco;
import com.gft.wallet.persist.models.Cliente;
import com.gft.wallet.persist.models.Tipoproducto;
import com.gft.wallet.persist.models.Transaccion;

public class TransaccionMapperTest {

	LocalTime hora = LocalTime.now();

	@Test
	public void toTransaccionDTO() {
		TransaccionDTO transaccionDTO = new TransaccionDTO("3698521047", LocalDate.now(), this.hora,
				"descripcion concepto", BigDecimal.valueOf(132.2), "Cuenta de referencia");
		Transaccion transaccion = new Transaccion("3698521047", LocalDate.now(), this.hora, "descripcion concepto",
				BigDecimal.valueOf(132.2), "Cuenta de referencia", null);
		assertTrue(EqualsBuilder.reflectionEquals(TransaccionMapper.INSTANCE.toTransaccionDTO(transaccion),
				transaccionDTO));
	}

	@Test
	public void toTransaccionSimple() {
		TransaccionDTO transaccionDTO = new TransaccionDTO("3698521047", LocalDate.now(), this.hora,
				"descripcion concepto", BigDecimal.valueOf(132.2), "Cuenta de referencia");
		Transaccion transaccion = new Transaccion("3698521047", LocalDate.now(), this.hora, "descripcion concepto",
				BigDecimal.valueOf(132.2), "Cuenta de referencia", null);
		assertTrue(EqualsBuilder.reflectionEquals(TransaccionMapper.INSTANCE.toTransaccionSimple(transaccionDTO),
				transaccion));
	}

	@Test
	public void toTransaccion() {
		Tipoproducto tipoproductobanco = new Tipoproducto("0987654321", "Debito", "premiun",
				new Banco("0147852369", "Santander Mx", "logo.icon"));
		Cliente cliente = new Cliente("3698521470", "Jasiel Cotrtez", "jocc@gft.com", "GFT");
		ProductoClienteTipoproductoBancoDTO productoClienteTipoproductoBancoDTO = new ProductoClienteTipoproductoBancoDTO(
				"1234567890123456", "alias", LocalDate.now(), "Inicial", "000",
				SimpleEntityMapper.INSTANCE.clienteToClienteDTO(cliente),
				TipoproductoMapper.INSTANCE.toTipoproductoBancoDTO(tipoproductobanco));
		TransaccionDTO transaccionDTO = new TransaccionDTO("3698521047", LocalDate.now(), this.hora,
				"descripcion concepto", BigDecimal.valueOf(132.2), "Cuenta de referencia");
		Transaccion transaccion = new Transaccion("3698521047", LocalDate.now(), this.hora, "descripcion concepto",
				BigDecimal.valueOf(132.2), "Cuenta de referencia",
				ProductoMapper.INSTANCE.toProducto(productoClienteTipoproductoBancoDTO));
		assertTrue(EqualsBuilder.reflectionEquals(
				TransaccionMapper.INSTANCE.toTransaccion(transaccionDTO, productoClienteTipoproductoBancoDTO),
				transaccion));
	}

	@Test
	public void toTransaccionProductoTipoproductoBancoDTO() {
		Tipoproducto tipoproductobanco = new Tipoproducto("0987654321", "Debito", "premiun",
				new Banco("0147852369", "Santander Mx", "logo.icon"));
		Cliente cliente = new Cliente("3698521470", "Jasiel Cotrtez", "jocc@gft.com", "GFT");
		ProductoTipoproductoBancoDTO productotipoproductoBancoDTO = new ProductoTipoproductoBancoDTO("1234567890123456",
				"alias", LocalDate.now(), "Inicial", "000",
				TipoproductoMapper.INSTANCE.toTipoproductoBancoDTO(tipoproductobanco));
		ProductoClienteTipoproductoBancoDTO productoClienteTipoproductoBancoDTO = new ProductoClienteTipoproductoBancoDTO(
				"1234567890123456", "alias", LocalDate.now(), "Inicial", "000",
				SimpleEntityMapper.INSTANCE.clienteToClienteDTO(cliente),
				TipoproductoMapper.INSTANCE.toTipoproductoBancoDTO(tipoproductobanco));
		TransaccionDTO transaccionDTO = new TransaccionDTO("3698521047", LocalDate.now(), this.hora,
				"descripcion concepto", BigDecimal.valueOf(132.2), "Cuenta de referencia");
		TransaccionProductoTipoproductoBancoDTO transaccionProductoTipoproductoBancoDTO = new TransaccionProductoTipoproductoBancoDTO(
				transaccionDTO, productotipoproductoBancoDTO);
		Transaccion transaccion = new Transaccion("3698521047", LocalDate.now(), this.hora, "descripcion concepto",
				BigDecimal.valueOf(132.2), "Cuenta de referencia",
				ProductoMapper.INSTANCE.toProducto(productoClienteTipoproductoBancoDTO));
		assertTrue(EqualsBuilder.reflectionEquals(
				TransaccionMapper.INSTANCE.toTransaccionProductoTipoproductoBancoDTO(transaccion),
				transaccionProductoTipoproductoBancoDTO));
	}

	@Test
	public void toTransaccionProductoClienteTipoproductoBancoDTO() {
		Tipoproducto tipoproductobanco = new Tipoproducto("0987654321", "Debito", "premiun",
				new Banco("0147852369", "Santander Mx", "logo.icon"));
		Cliente cliente = new Cliente("3698521470", "Jasiel Cotrtez", "jocc@gft.com", "GFT");
		ProductoClienteTipoproductoBancoDTO productoClienteTipoproductoBancoDTO = new ProductoClienteTipoproductoBancoDTO(
				"1234567890123456", "alias", LocalDate.now(), "Inicial", "000",
				SimpleEntityMapper.INSTANCE.clienteToClienteDTO(cliente),
				TipoproductoMapper.INSTANCE.toTipoproductoBancoDTO(tipoproductobanco));
		TransaccionDTO transaccionDTO = new TransaccionDTO("3698521047", LocalDate.now(), this.hora,
				"descripcion concepto", BigDecimal.valueOf(132.2), "Cuenta de referencia");
		Transaccion transaccion = new Transaccion("3698521047", LocalDate.now(), this.hora, "descripcion concepto",
				BigDecimal.valueOf(132.2), "Cuenta de referencia",
				ProductoMapper.INSTANCE.toProducto(productoClienteTipoproductoBancoDTO));
		TransaccionProductoClienteTipoproductoBancoDTO transaccionProductoClienteTipoproductoBancoDTO = new TransaccionProductoClienteTipoproductoBancoDTO(
				transaccionDTO, productoClienteTipoproductoBancoDTO);
		assertTrue(EqualsBuilder.reflectionEquals(
				TransaccionMapper.INSTANCE.toTransaccionProductoClienteTipoproductoBancoDTO(transaccion),
				transaccionProductoClienteTipoproductoBancoDTO));
	}

}
