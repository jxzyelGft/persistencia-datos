package com.gft.wallet.persist.mapper;

import static org.junit.Assert.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.junit.Test;
import com.gft.wallet.commons.model.BancoDTO;
import com.gft.wallet.commons.model.ClienteDTO;
import com.gft.wallet.persist.models.Banco;
import com.gft.wallet.persist.models.Cliente;

public class SimpleEntityMapperTest {

	@Test
	public void clienteToClienteDTO() {
		Cliente cliente = new Cliente("1234567890", "Nombre Apellidos", "mail@dominio.com", "qwertyuiop");
		ClienteDTO clienteDTO = new ClienteDTO("1234567890", "Nombre Apellidos", "mail@dominio.com", "qwertyuiop");
		assertTrue(
				EqualsBuilder.reflectionEquals(SimpleEntityMapper.INSTANCE.clienteToClienteDTO(cliente), clienteDTO));
	}

	@Test
	public void clienteDTOToCliente() {
		Cliente cliente = new Cliente("1234567890", "Nombre Apellidos", "mail@dominio.com", "qwertyuiop");
		ClienteDTO clienteDTO = new ClienteDTO("1234567890", "Nombre Apellidos", "mail@dominio.com", "qwertyuiop");
		assertTrue(
				EqualsBuilder.reflectionEquals(SimpleEntityMapper.INSTANCE.clienteDTOToCliente(clienteDTO), cliente));
	}

	@Test
	public void bancoToBancoDTO() {
		Banco banco = new Banco("1234567890", "Nombre del banco", "icon.png");
		BancoDTO bancoDTO = new BancoDTO("1234567890", "Nombre del banco", "icon.png");
		assertTrue(EqualsBuilder.reflectionEquals(SimpleEntityMapper.INSTANCE.bancoToBancoDTO(banco), bancoDTO));
	}

	@Test
	public void bancoDTOToBanco() {
		Banco banco = new Banco("1234567890", "Nombre del banco", "icon.png");
		BancoDTO bancoDTO = new BancoDTO("1234567890", "Nombre del banco", "icon.png");
		assertTrue(EqualsBuilder.reflectionEquals(SimpleEntityMapper.INSTANCE.bancoDTOToBanco(bancoDTO), banco));
	}

}
