package com.gft.wallet.persist.mapper;

import static org.junit.Assert.assertTrue;
import java.time.LocalDate;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.junit.Test;
import com.gft.wallet.commons.model.ProductoClienteTipoproductoBancoDTO;
import com.gft.wallet.commons.model.ProductoDTO;
import com.gft.wallet.commons.model.ProductoTipoproductoBancoDTO;
import com.gft.wallet.persist.models.Banco;
import com.gft.wallet.persist.models.Cliente;
import com.gft.wallet.persist.models.Producto;
import com.gft.wallet.persist.models.Tipoproducto;

public class ProductoMapperTest {

	@Test
	public void toProducto3Objs() {
		Tipoproducto tipoproductobanco = new Tipoproducto("0987654321", "Debito", "premiun",
				new Banco("0147852369", "Santander Mx", "logo.icon"));
		Cliente cliente = new Cliente("3698521470", "Jasiel Cotrtez", "jocc@gft.com", "GFT");
		Producto producto = new Producto("1234567890123456", "alias", LocalDate.now(), "Inicial", "000",
				tipoproductobanco, cliente);
		ProductoDTO productoDTO = new ProductoDTO("1234567890123456", "alias", LocalDate.now(), "Inicial", "000");
		assertTrue(EqualsBuilder.reflectionEquals(ProductoMapper.INSTANCE.toProducto(productoDTO,
				TipoproductoMapper.INSTANCE.toTipoproductoBancoDTO(tipoproductobanco),
				SimpleEntityMapper.INSTANCE.clienteToClienteDTO(cliente)), producto));
	}

	@Test
	public void toProducto2Objs() {
		Tipoproducto tipoproductobanco = new Tipoproducto("0987654321", "Debito", "premiun",
				new Banco("0147852369", "Santander Mx", "logo.icon"));
		Cliente cliente = new Cliente("3698521470", "Jasiel Cotrtez", "jocc@gft.com", "GFT");
		Producto producto = new Producto("1234567890123456", "alias", LocalDate.now(), "Inicial", "000",
				tipoproductobanco, cliente);
		ProductoTipoproductoBancoDTO productotipoproductoBancoDTO = new ProductoTipoproductoBancoDTO("1234567890123456",
				"alias", LocalDate.now(), "Inicial", "000",
				TipoproductoMapper.INSTANCE.toTipoproductoBancoDTO(tipoproductobanco));
		assertTrue(EqualsBuilder.reflectionEquals(ProductoMapper.INSTANCE.toProducto(productotipoproductoBancoDTO,
				SimpleEntityMapper.INSTANCE.clienteToClienteDTO(cliente)), producto));
	}

	@Test
	public void toProducto1Obj() {
		Tipoproducto tipoproductobanco = new Tipoproducto("0987654321", "Debito", "premiun",
				new Banco("0147852369", "Santander Mx", "logo.icon"));
		Cliente cliente = new Cliente("3698521470", "Jasiel Cotrtez", "jocc@gft.com", "GFT");
		Producto producto = new Producto("1234567890123456", "alias", LocalDate.now(), "Inicial", "000",
				tipoproductobanco, cliente);
		ProductoClienteTipoproductoBancoDTO productoClienteTipoproductoBancoDTO = new ProductoClienteTipoproductoBancoDTO(
				"1234567890123456", "alias", LocalDate.now(), "Inicial", "000",
				SimpleEntityMapper.INSTANCE.clienteToClienteDTO(cliente),
				TipoproductoMapper.INSTANCE.toTipoproductoBancoDTO(tipoproductobanco));
		assertTrue(EqualsBuilder
				.reflectionEquals(ProductoMapper.INSTANCE.toProducto(productoClienteTipoproductoBancoDTO), producto));
	}

	@Test
	public void toProductoTipoproductoBancoDTO() {
		Tipoproducto tipoproductobanco = new Tipoproducto("0987654321", "Debito", "premiun",
				new Banco("0147852369", "Santander Mx", "logo.icon"));
		Cliente cliente = new Cliente("3698521470", "Jasiel Cotrtez", "jocc@gft.com", "GFT");
		
		Producto producto = new Producto("1234567890123456", "alias", LocalDate.now(), "Inicial", "000",
				tipoproductobanco, cliente);
		ProductoTipoproductoBancoDTO productotipoproductoBancoDTO = new ProductoTipoproductoBancoDTO("1234567890123456",
				"alias", LocalDate.now(), "Inicial", "000",
				TipoproductoMapper.INSTANCE.toTipoproductoBancoDTO(tipoproductobanco));
		assertTrue(EqualsBuilder.reflectionEquals(ProductoMapper.INSTANCE.toProductoTipoproductoBancoDTO(producto),
				productotipoproductoBancoDTO));
	}

	@Test
	public void toProductoTipoproductoBancoDTOFromDTO() {
		Tipoproducto tipoproductobanco = new Tipoproducto("0987654321", "Debito", "premiun",
				new Banco("0147852369", "Santander Mx", "logo.icon"));
		Cliente cliente = new Cliente("3698521470", "Jasiel Cotrtez", "jocc@gft.com", "GFT");
		ProductoTipoproductoBancoDTO productotipoproductoBancoDTO = new ProductoTipoproductoBancoDTO("1234567890123456",
				"alias", LocalDate.now(), "Inicial", "000",
				TipoproductoMapper.INSTANCE.toTipoproductoBancoDTO(tipoproductobanco));
		ProductoClienteTipoproductoBancoDTO productoClienteTipoproductoBancoDTO = new ProductoClienteTipoproductoBancoDTO(
				"1234567890123456", "alias", LocalDate.now(), "Inicial", "000",
				SimpleEntityMapper.INSTANCE.clienteToClienteDTO(cliente),
				TipoproductoMapper.INSTANCE.toTipoproductoBancoDTO(tipoproductobanco));
		assertTrue(EqualsBuilder.reflectionEquals(
				ProductoMapper.INSTANCE.toProductoTipoproductoBancoDTO(productoClienteTipoproductoBancoDTO),
				productotipoproductoBancoDTO));
	}

	@Test
	public void toProductoClienteTipoproductoBancoDTO() {
		Tipoproducto tipoproductobanco = new Tipoproducto("0987654321", "Debito", "premiun",
				new Banco("0147852369", "Santander Mx", "logo.icon"));
		Cliente cliente = new Cliente("3698521470", "Jasiel Cotrtez", "jocc@gft.com", "GFT");
		Producto producto = new Producto("1234567890123456", "alias", LocalDate.now(), "Inicial", "000",
				tipoproductobanco, cliente);
		ProductoClienteTipoproductoBancoDTO productoClienteTipoproductoBancoDTO = new ProductoClienteTipoproductoBancoDTO(
				"1234567890123456", "alias", LocalDate.now(), "Inicial", "000",
				SimpleEntityMapper.INSTANCE.clienteToClienteDTO(cliente),
				TipoproductoMapper.INSTANCE.toTipoproductoBancoDTO(tipoproductobanco));
		assertTrue(
				EqualsBuilder.reflectionEquals(ProductoMapper.INSTANCE.toProductoClienteTipoproductoBancoDTO(producto),
						productoClienteTipoproductoBancoDTO));
	}

	@Test
	public void toProductoDTO() {
		Tipoproducto tipoproductobanco = new Tipoproducto("0987654321", "Debito", "premiun",
				new Banco("0147852369", "Santander Mx", "logo.icon"));
		Cliente cliente = new Cliente("3698521470", "Jasiel Cotrtez", "jocc@gft.com", "GFT");
		Producto producto = new Producto("1234567890123456", "alias", LocalDate.now(), "Inicial", "000",
				tipoproductobanco, cliente);
		ProductoDTO productoDTO = new ProductoDTO("1234567890123456", "alias", LocalDate.now(), "Inicial", "000");
		assertTrue(EqualsBuilder.reflectionEquals(ProductoMapper.INSTANCE.toProductoDTO(producto), productoDTO));
	}

}
