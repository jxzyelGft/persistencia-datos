package com.gft.wallet.persist.mapper;

import static org.junit.Assert.*;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.junit.Test;
import com.gft.wallet.commons.model.TipoproductoBancoDTO;
import com.gft.wallet.commons.model.TipoproductoDTO;
import com.gft.wallet.persist.models.Banco;
import com.gft.wallet.persist.models.Tipoproducto;

public class TipoproductoMapperTest {

	@Test
	public void toTipoproducto2Objs() {
		TipoproductoDTO tipoproductoDTO = new TipoproductoDTO("0987654321", "Debito", "premiun");
		Banco banco = new Banco("0147852369", "Santander Mx", "logo.icon");
		Tipoproducto tipoproducto = new Tipoproducto("0987654321", "Debito", "premiun", banco);
		assertTrue(EqualsBuilder.reflectionEquals(TipoproductoMapper.INSTANCE.toTipoproducto(tipoproductoDTO, banco),
				tipoproducto));
	}

	@Test
	public void toTipoproducto1Obj() {
		Banco banco = new Banco("0147852369", "Santander Mx", "logo.icon");
		TipoproductoBancoDTO tipoproductobancoDTO = new TipoproductoBancoDTO("0987654321", "Debito", "premiun",
				SimpleEntityMapper.INSTANCE.bancoToBancoDTO(banco));
		Tipoproducto tipoproducto = new Tipoproducto("0987654321", "Debito", "premiun", banco);
		assertTrue(EqualsBuilder.reflectionEquals(TipoproductoMapper.INSTANCE.toTipoproducto(tipoproductobancoDTO),
				tipoproducto));
	}

	@Test
	public void toTipoproductoDTO() {
		TipoproductoDTO tipoproductoDTO = new TipoproductoDTO("0987654321", "Debito", "premiun");
		Tipoproducto tipoproducto = new Tipoproducto("0987654321", "Debito", "premiun", null);
		assertTrue(EqualsBuilder.reflectionEquals(TipoproductoMapper.INSTANCE.toTipoproductoDTO(tipoproducto),
				tipoproductoDTO));
	}

	@Test
	public void toTipoproductoBancoDTO() {
		Banco banco = new Banco("0147852369", "Santander Mx", "logo.icon");
		Tipoproducto tipoproducto = new Tipoproducto("1234567890", "Credito", "Platino", banco);
		TipoproductoBancoDTO tipoproductobancoDTO = new TipoproductoBancoDTO("1234567890", "Credito", "Platino",
				SimpleEntityMapper.INSTANCE.bancoToBancoDTO(banco));
		assertTrue(EqualsBuilder.reflectionEquals(TipoproductoMapper.INSTANCE.toTipoproductoBancoDTO(tipoproducto),
				tipoproductobancoDTO));
	}

}
